const mongoose = require('mongoose')

const Schema = mongoose.Schema;


const StatisticsSchema = new Schema(
    {
        population: {type: String},
        military: {type: String},
        economy: {type: String},
        
    }                   /**Schema.Types.ObjectId es lo mismo */
    ,
    {
        timestamps: true,
    }
);

const Statistics = mongoose.model('Statistics', StatisticsSchema);
module.exports = Statistics;