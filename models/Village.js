const mongoose = require('mongoose')

const Schema = mongoose.Schema;

const villageSchema = new Schema( 
    {
        name: {type: String, required: true},
        country: {type: String, required: true}, 
        leader: {type: String, required: true},
        statistics: [{type: Schema.Types.ObjectId, ref: 'Statistics' }],
        image : {type: String}
    },
    {
        timestamps:true
    })

    const Village = mongoose.model('Village', villageSchema);
    module.exports = Village