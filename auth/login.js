const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const User = require('../models/User');
const bcrypt = require('bcrypt');

const loginStrategy = new LocalStrategy(
    {
        usernameField: 'email', 
        passwordField: 'password',
        passReqToCallback: true,
    },
    async (req, email, password, done) => {
        try {
            const currentUser = await User.findOne({ email: email });
            // Si hay usuario previamente, lanzamos un error
            if(!currentUser) {
                const error = new Error('The user does not exist!');
                return done(error);
            }
            // Si existe el usuario, vamos a comprobar si su password enviado coincide con el registrado.
            const isValidPassword = await bcrypt.compare(
                password,
                currentUser.password
            );

            // Si el password no es correcto, enviamos un error a nuestro usuario
            if (!isValidPassword) {
                const error = new Error('The email & password combination is incorrect!');
                return done(error)
            }

            // Si todo se valida correctamente, eliminamos la contraseña del usuario devuelto
            // por la db y completamos el callback con el usuario
            currentUser.password = null
            return done(null, currentUser)
        } catch (error) {
            return done(error)
        }
    }
)


module.exports = loginStrategy