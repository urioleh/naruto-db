const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const User = require('../models/User');
const bcrypt = require('bcrypt');


const validateEmail = (email) => {
    const re = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

    return re.test(String(email).toLowerCase())
};

const validatePassword = (password) => {
    var re = /^(?=.*[0-9])[a-zA-Z0-9!@#$%^&*]{6,16}$/;

    return re.test(String(password))
}


const registerStrategy = new LocalStrategy(
    {
        usernameField: 'email', // Elegimos el campo email del req.body
        passwordField: 'password', // Elegimos el campo password del req.body
        passReqToCallback: true,// Hace que el callback reciba la Request (req)
    },
    async (req, email, password, done) => {

            try {
            const isValidEmail = validateEmail(email)
            const isValidPassword = validatePassword(password)

            if (!isValidEmail) {
                const error = new Error('The email is not valid')
                return done(error)
            }

            if (!isValidPassword) {
                const error = new Error('The password is not valid')
                return done(error)
            }

            // Primero buscamos si el email o name existes en nuestra DB
            const { name } = req.body;
            const existingUser = await User.findOne({ email: email });
            const existingName = await User.findOne({ name: name })
            // Si hay usuario previamente, lanzamos un error
            if(existingUser) {
                const error = new Error('This email is already registered!');
                return done(error);
            } else if (existingName) {
                const error = new Error('This name is already registered!');
                return done(error);
            }
            // Creamos los salts de bcrypt, a mas salts más seguro pero más lento
            const saltRounds = 10;
             // Si no existe el usuario, vamos a "hashear" el password antes de registrarlo
            const hash = await bcrypt.hash(password, saltRounds);
            
            // Creamos el nuevo user y lo guardamos en la DB
            const newUser = new User({
                email: email,
                password: hash,
                name: name, //Lo he destructurado antes
                role: 'user',
            });

            const savedUser = await newUser.save();

            // Invocamos el callback con null donde iría el error y el usuario creado
            done(null, savedUser)
            } catch (error) {
            return done(error);
        }
    }
);


// Nombre de la estrategia, en este caso será register
module.exports = registerStrategy 