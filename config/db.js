const mongoose = require('mongoose');
require('dotenv').config();

// const DB_URL = "mongodb://127.0.0.1:27017/naruto-database"
const DB_URL = process.env.DB_URL_PRODUCTION;
const CONFIG_DB = {useNewUrlParser: true, useUnifiedTopology: true}

const connectToDb = async () => {
    try {
        const response = await mongoose.connect(DB_URL, CONFIG_DB)
        const { host, port, name} = response.connection;
        console.log(`Conectado a ${name} en ${host}:${port}`)
    } catch (error) {
        console.log(`Error del catch: ${error}`)
    }
}

module.exports = { DB_URL, CONFIG_DB, connectToDb };