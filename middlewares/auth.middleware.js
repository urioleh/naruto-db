const isAuth = ((req, res, next) => {
    if (req.isAuthenticated()) {
        return next();
    } else {
        return res.status(401).json('No estas logueado')
    }
})

const isAdmin = ((req, res, next) => {
    /**
     * 1.Comprobamos si está autenticado.
     *      NO: enviamos 401
     *      SI: Comprobamos que tenga el role = admin or
     */
    if (req.isAuthenticated()) {
        // Está autenticado

        if (req.user.role === 'admin') {
        // es administrador
        return next();
        } else {
            // está autenticado pero no es admin
            return res.status(403).json('No eres administrador del mejor servidor del mundo!')
        } 
    } else {
        // No está autenticado
        return res.status(401).json('No está autenticado')
    }
})

module.exports ={ isAuth, isAdmin }