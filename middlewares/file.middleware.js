const path = require('path');
const fs = require('fs');
const multer = require('multer');
const cloudinary = require('cloudinary').v2; //Cloudinary dice que se ponga ese v2


const storage = multer.diskStorage({
    filename: (req, file, callback) => {
        callback(null, `${Date.now()}-${file.originalname}`)
    },
    destination: (req, file, callback) => {
        const directory = path.join(__dirname,'../public/uploads/');
        callback(null, directory)
    },
});

const ACCEPTED_FILE_EXTENSIONS = ['image/png', 'image/jpeg', 'image/jpg']

const fileFilter = (req, file, callback) => {
    /**
    * 1. Si el tipo de archivo no coincide con inguno de nuestro array de ACCEPTED, sacaremos un error
    */
    if(ACCEPTED_FILE_EXTENSIONS.includes(file.mimetype)) {
    // es un mimetype valido
        callback(null, true)
    } else {
    // no es un mimetype valido --> Rechazamos la subida de archivo
        const error = new Error('Invalid file type')
        error.status = 400; //Bad Request
        callback(error, null)
    }
};

const upload = multer({
    storage,
    fileFilter,
});

const uploadToCloudinary = async (req, res, next) => {
    if (req.file) {
        console.log('Subiendo a Cloudinary...');
    
        const filePath = req.file.path;
        const imageFromCloudinary = await cloudinary.uploader.upload(filePath);

        console.log('Imagen subida con éxito', imageFromCloudinary)

        req.avatarFromCloudinary = imageFromCloudinary.secure_url;

        return next()
    } else {
        return next()
    }
}

module.exports = { upload, uploadToCloudinary };