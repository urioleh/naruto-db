const express = require('express');
const Statistics = require('../models/Statistics');

const router = express.Router();

router.get('/', async (req, res, next) => {
    try {
        const statistics = await Statistics.find()
        return res.status(200).json(statistics)
    } catch (err) {
        return next(err);
    }
})

module.exports = router