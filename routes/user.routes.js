const express = require('express');
const passport = require('passport');
const User = require('../models/User')

const router = express.Router();

router.post('/register', (req, res, next) => {
    const done = (error, user) => {
        // Si hay un error llamaremos a nuestro controlador de errores
        if(error) {
            return next(error);
        }

        req.logIn(user, (error) => {
            // Si hay un error logeando al usuario, resolvemos el controlador
            if (error) return next(error);
            
            // Si no hay error, devolvemos al usuario registrado y logueado
            return res.status(201).json(user)
        })
    }
    // Invocamos a la autenticación de Passport parapassport
    passport.authenticate('register',done)(req);
})


router.post('/login', (req, res, next) => {
    const done = (error, user) => {
        if (error) return next(error)

        req.logIn(user, (error) => {
            // Si hay un error logeando al usuario, resolvemos el controlador
            if (error) return next(error);
            
            // Si no hay error, devolvemos al usuario logueado
            return res.status(200).json(user)
        })
    }
    
    passport.authenticate('login', done)(req)
})


router.post('/logout', (req, res, next) => {
    if (req.user) {
        //Deslogueo al usuario

        req.logout();

        req.session.destroy(() => {
            res.clearCookie('connect.sid');

            return res.status(200).json('Usuario deslogueado')
        })
        
    } else{
        const error = new Error('No existe usuario logueado')
        return next(error)
    }
})


module.exports = router;