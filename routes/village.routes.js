const express = require('express');
const Village = require('../models/Village');
const { upload, uploadToCloudinary } = require('../middlewares/file.middleware');
const { isAuth, isAdmin } = require('../middlewares/auth.middleware');

const router = express.Router();

const middlewarePropio = (req, res, next) => {
    console.log('Esto es un middleware propio de la ruta --> villages.routes.js')

    return next()
}



router.get('/', [isAuth, middlewarePropio], async (req, res, next) => {
    try {
        console.log('req.oriol --> endpoint',req.oriol);
        const villages = await Village.find().populate('statistics','population military economy')
        return res.status(200).json(villages)
    } catch (error) {
        return next(error)
    }
})

router.get('/name/:name', async (req, res, next)=> {
    try {
        const { name } = req.params
        const village = await Village.find({
            name: {
              $regex: new RegExp("^" + name.toLowerCase(), "i"), //Buscar case insensitive
            },
          }).populate('statistics','population military economy')
          console.log("character -->", village);
          return res.status(200).json(village)
    } catch (error) {
        return next(error)
    }
})

router.post('/create', [isAdmin, upload.single('image'), uploadToCloudinary], async (req, res, next) => {
    try {
        const avatar = req.avatarFromCloudinary ? req.avatarFromCloudinary : null;

        const { name, country, leader, statistics, image} = req.body;
        const newVillage = new Village({ ...req.body, image: avatar})
        const createdVillage = await newVillage.save()
        return res.status(200).json({message:'Village created succesfully', data: createdVillage})
    } catch (error) {
        return next(error)
    }
})

router.delete('/:id', async (req, res, next)=> {
    try {
        const { id } = req.params;
        const deleted = await Village.findByIdAndDelete(id);
        if (deleted) {
            return res.status(200).json({ message: 'Village deleted', data: deleted })
        } else {
            return res.status(404).json({ message: "Can't find this id", id: id })
        }
    } catch (error) {
        return next(error)
    }

})

router.put('/edit/:id', async (req, res, next) => {
    try {
        const { id } = req.params;
        const villageModify = new Village(req.body)
        villageModify._id = id;
        const villageUpdated = await Village.findByIdAndUpdate(id, villageModify, {new: true});
        return res.status(200).json({message: `${villageUpdated.name} updated succesfully`, data: villageUpdated})
    } catch (err) {
        return next(err)
    }
})

module.exports = router