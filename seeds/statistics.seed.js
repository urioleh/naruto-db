const mongoose = require('mongoose');
const Statistics = require('../models/Statistics');
const { DB_URL, CONFIG_DB } = require('../config/db');

console.log(DB_URL)
console.log(CONFIG_DB)

const statisticsArray = [
    {
        population: "5/5",
        military: "3/5",
        economy: "2/5",
        
    },
    {
        population: "2/5",
        military: "3/5",
        economy: "2/5",
        
    },  {
        population: "3/5",
        military: "4/5",
        economy: "5/5",
        
    },  {
        population: "2/5",
        military: "2/5",
        economy: "4/5",
        
    },  {
        population: "4/5",
        military: "4/5",
        economy: "2/5",
        
    },  {
        population: "1/5",
        military: "2/5",
        economy: "1/5",
        
    },

];

mongoose.connect(DB_URL, CONFIG_DB)
.then(async()=> {
    console.log('Executing Statistics seed')
    const allStatistics = await Statistics.find();

    if (allStatistics.lenght) {
        await Statistics.collection.drop();
    }
})
.catch(err => console.log('Error searching in DB', err))
.then(async() => {
    await Statistics.insertMany(statisticsArray);
    console.log('New Statistics added into DB')
})
.catch(err => console.log('Error adding Statistics', err))
.finally(() => mongoose.disconnect());