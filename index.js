// modules
const express = require('express');
require('dotenv').config();
const passport = require('passport');
const session = require('express-session');
const MongoStore = require('connect-mongo');
// routes
const routes = require('./routes/html.routes');
const villageRouter = require('./routes/village.routes');
const statisticsRouter = require('./routes/statistics.routes');
const userRouter = require('./routes/user.routes');
// middlewares for authentification
const { isAdmin } = require('./middlewares/auth.middleware')
require('./auth')  //index for authentification

// Conectar DataBase
const { connectToDb, DB_URL } = require('./config/db');
connectToDb()


const PORT = process.env.PORT || 4000;
server = express();

// EJEMPLO DE MIDDLEWARE CREADO
server.use((req, res, next) => {
    console.log('HOLA SOY UN MIDDLEWARE --> index.js');
    req.oriol = 'Hola, soy tu padre';

    return next();
})


// Configuración de sesión de login
server.use(
    session({ 
        secret : process.env.SESSION_SECRET,
        resave: false, // Solo guardará la sesión si hay cambios en ella.
        saveUninitialized: false, // Lo usaremos como false debido a que gestionamos nuestra sesión con Passport
        cookie: {
            maxAge: 24 * 60 * 60 * 1000 // Un dia
        },
        store :MongoStore.create({mongoUrl: DB_URL})
    })
); 

// Modificar servidor para inicializar passport(Registrar usuarios)
server.use(passport.initialize())
server.use(passport.session()) //Transformar cookie de sesión en un usuario llamando a serialize i deserialize

// Modificar servidor para interpretar y meter info en req.body
server.use(express.json());
server.use(express.urlencoded({ extended: false }));

// Routear endpoints
server.use('/', routes)
server.use('/villages',villageRouter);
server.use('/statistics', [isAdmin],statisticsRouter); //El isAdmin llama para toda la ruta al middleware de la carpeta middlewares que mira si eres admin.
server.use('/users',userRouter);

// server.get('/', function(req, res) {
//     res.send('Naruto server!');
//   });


//Creamos un static despues vincular la carpeta public con el index.html
server.use(express.static(__dirname + '/public'))


// Modificar servidor para controlar errores
server.use((error, req, res, next) => {
    const status = error.status || 500;
    const message = error.message || 'Unexpected error, buddy!'

    return res.status(status).json(message);
})

const serverCallback = () => console.log(`Servidor funcionando en localhost:${PORT}`)

// Escuchar servidor y enviar puerto y callback
server.listen(PORT, serverCallback)

